
typedef struct UltraSonicConfig{
  int trig;
  int echo;
}UltraSonicConfig;


//connect ultrasonic trig to pin 11, echo to pin 12 of Arduino
UltraSonicConfig ultraSonicSensor1={.trig=11, .echo=12};

void setupUltraSonicSensor(UltraSonicConfig *ultraSonicSensor);
int measureDistance(UltraSonicConfig *ultraSonicSensor);

int previousValue=-1;

void setup() {

setupUltraSonicSensor(&ultraSonicSensor1);
Serial.begin(9600);
}



void loop()
{

  int value=measureDistance(&ultraSonicSensor1);
  if(value != previousValue)
  {
   Serial.println(value);
  previousValue=value;

  }
delay(100);
}


/**
 * return distance in cm, from SR04 sensor connected on ports trig and echo
 */
int measureDistance(UltraSonicConfig *ultraSonicSensor)
{
digitalWrite(ultraSonicSensor->trig, LOW);
delayMicroseconds(2);
//trigger is a 5 microsecond pulse
digitalWrite(ultraSonicSensor->trig, HIGH);
delayMicroseconds(5);
digitalWrite(ultraSonicSensor->trig, LOW);

//read the duration in microseconds from the echo pin
long duration = pulseIn(ultraSonicSensor->echo, HIGH);

//speed of sound in air, 340m/s, 29microsecs/cm
return duration/29/2;
}


void setupUltraSonicSensor(UltraSonicConfig *ultraSonicSensor)
{
//setup i/o ports of the ultrasonic sensor
pinMode(ultraSonicSensor->echo,INPUT);
pinMode(ultraSonicSensor->trig,OUTPUT);
}

