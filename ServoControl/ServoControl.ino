
#include <Servo.h>

#define SERVO_PIN 9 // connect signal line of servo to Digital pin 9 on UNO
#define POT_PIN 0 // connect wiper (middle pin) of potentiometer to Analog ping 0

Servo servo1;

void setup() {

  Serial.println("ServoControl");

  servo1.attach(SERVO_PIN);

}

void loop() {
   int i = analogRead(POT_PIN);
   Serial.println(i);

   //map analog port resolved value (range 1024), to servo range of 180 degrees
   int angle = map(i, 0, 1023, 0, 180);
   servo1.write(angle);

   //delay for servo to actuate
   delay(15);
}

