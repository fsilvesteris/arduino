

// I2C communication is provided by the "Wire" library
#include <Wire.h>

#include <LiquidCrystal_I2C.h>

/* CONNECTIONS
 *  
 Arduino (Uno,Nano) 
  A4 SCA (datA)
  A5 SCL (cLock)
 */
// THE I2C ADDRESS OF THIS LCD 0x3F (or 0x27) 

#define LCD_I2C_ADDR  0x3F
//#define LCD_I2C_ADDR  0x27
#define BACKLIGHT_PIN 3
#define En_pin  2
#define Rw_pin  1
#define Rs_pin  0
#define D4_pin  4
#define D5_pin  5
#define D6_pin  6
#define D7_pin  7

//initialise lcd configuration
LiquidCrystal_I2C lcdDef(LCD_I2C_ADDR,En_pin,Rw_pin,Rs_pin,D4_pin,D5_pin,D6_pin,D7_pin,BACKLIGHT_PIN, POSITIVE);

LCD *lcd = &lcdDef;







typedef struct UltraSonicConfig{
  int trig;
  int echo;
}UltraSonicConfig;


void setupUltraSonicSensor(UltraSonicConfig *ultraSonicSensor);
int measureDistance(UltraSonicConfig *ultraSonicSensor);


//connect ultrasonic trig to pin 11, echo to pin 12 of Arduino
UltraSonicConfig ultraSonicSensor1={.trig=11, .echo=12};

int previousValue=-1;

boolean lcdPresent=false;

void setup() {
Wire.begin();

delay(500);

Wire.beginTransmission(LCD_I2C_ADDR);
lcdPresent=Wire.endTransmission() == 0;
if(lcdPresent)
{
  lcd->begin(20, 4, LCD_5x8DOTS);
  lcd->display();
  lcd->setBacklight(1);
  lcd->clear();
  lcd->setCursor(0,0);
  lcd->print("Ultrasonic");
}


setupUltraSonicSensor(&ultraSonicSensor1);

Serial.begin(9600);
}



void loop()
{

  int value=measureDistance(&ultraSonicSensor1);
  if(value != previousValue)
  {
   Serial.println(value);

    if(lcdPresent)
    {
      lcd->setCursor(7,2);
      lcd->print(value);
      lcd->print("   ");
    }
   
  previousValue=value;

  }
delay(250);
}



/**
 * return distance in cm, from SR04 sensor connected on ports trig and echo
 */
int measureDistance(UltraSonicConfig *ultraSonicSensor)
{
digitalWrite(ultraSonicSensor->trig, LOW);
delayMicroseconds(2);
//trigger is a 5 microsecond pulse
digitalWrite(ultraSonicSensor->trig, HIGH);
delayMicroseconds(5);
digitalWrite(ultraSonicSensor->trig, LOW);

//read the duration in microseconds from the echo pin
long duration = pulseIn(ultraSonicSensor->echo, HIGH);

//speed of sound in air, 340m/s, 29microsecs/cm
return duration/29/2;
}


void setupUltraSonicSensor(UltraSonicConfig *ultraSonicSensor)
{
//setup i/o ports of the ultrasonic sensor
pinMode(ultraSonicSensor->echo,INPUT);
pinMode(ultraSonicSensor->trig,OUTPUT);
}


