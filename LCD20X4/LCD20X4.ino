

// I2C communication is provided by the "Wire" library
#include <Wire.h>

/*
//use the new liquid crystal library from
https://bitbucket.org/fmalpartida/new-liquidcrystal/wiki/Home

download the latest version from
https://bitbucket.org/fmalpartida/new-liquidcrystal/downloads

e.g.download
https://bitbucket.org/fmalpartida/new-liquidcrystal/downloads/NewliquidCrystal_1.3.4.zip

then add it to the IDE...

[Sketch][Include Library][Add .ZIP Library]

and included using
#include <LiquidCrystal_I2C.h>
*/

#include <LiquidCrystal_I2C.h>

/* CONNECTIONS
 *  
 Arduino (Uno,Nano) 
  A4 SCA (datA)
  A5 SCL (cLock)
 */


/*
 Connect SCA to A4, SCL to A5  (Arduino Uno/Nano)
*/

// THE I2C ADDRESS OF THIS LCD 0x3F (or 0x27) - if not found, run I2CScanner 

#define LCD_I2C_ADDR  0x3F
//#define LCD_I2C_ADDR  0x27
#define BACKLIGHT_PIN 3
#define En_pin  2
#define Rw_pin  1
#define Rs_pin  0
#define D4_pin  4
#define D5_pin  5
#define D6_pin  6
#define D7_pin  7

//initialise lcd configuration
LiquidCrystal_I2C lcdDef(LCD_I2C_ADDR,En_pin,Rw_pin,Rs_pin,D4_pin,D5_pin,D6_pin,D7_pin,BACKLIGHT_PIN, POSITIVE);

LCD *lcd = &lcdDef;

int counter=0;

void setup() {

Wire.begin();

delay(500);

Wire.beginTransmission(LCD_I2C_ADDR);

boolean lcdPresent=Wire.endTransmission() == 0;

if(lcdPresent)
{
lcd->begin(20, 4, LCD_5x8DOTS);

lcd->display();

lcd->setBacklight(1);

lcd->clear();
}else{
 Serial.begin(9600);
 while (!Serial) ; // wait for serial monitor to be connected
  
 delay(1000);
 Serial.println("LCD not found at address");
 Serial.println(LCD_I2C_ADDR);
 Serial.println(" check connections and run I2CScanner to confirm address");
}


delay(1000);

}


void loop() {

  lcd->setCursor(0,0);
  lcd->print("20X4 Character LCD");

  lcd->setCursor(0,1);
  lcd->print("I2C address ");
  lcd->print(LCD_I2C_ADDR,16);

  lcd->setCursor(0,2);
  lcd->print("LiquidCrystal_I2C");

  lcd->setCursor(0,3);
  lcd->print(counter++);

  delay(1000);
}
